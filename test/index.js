const expect  = require('chai').expect;
const request = require('request');

it('Main page content', function(done) {
  request('http://localhost:3000/' , function(error, response, body) {
    expect(body).to.equal('Hello World!');
    done();
  });
});

it('Main page content 200', function(done) {
  request('http://localhost:3000/' , function(error, response, body) {
    expect(response.statusCode).to.equal(200);
    done();
  });
});

it('About page content', function(done) {
  request('http://localhost:3000/about' , function(error, response, body) {
    expect(response.statusCode).to.equal(404);
    done();
  });
});